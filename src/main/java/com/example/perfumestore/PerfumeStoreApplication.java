package com.example.perfumestore;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PerfumeStoreApplication {

	public static void main(String[] args) {
		SpringApplication.run(PerfumeStoreApplication.class, args);
	}

}
