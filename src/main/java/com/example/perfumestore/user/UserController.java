package com.example.perfumestore.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.web.bind.annotation.*;

import javax.annotation.security.RolesAllowed;
import java.time.LocalDate;
import java.util.List;
import java.util.Set;

@RestController
@RequestMapping(path = "api/user")
public class UserController {

    private final UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @Secured({"ADMIN"})
    @GetMapping
    public List<User> getUsers() {
        return userService.getUsers();
    }

    @Secured({"ADMIN", "CUSTOMER"})
    @GetMapping(path = "me")
    public User getMyUser(@AuthenticationPrincipal User user) {
//        return userService.getUser(user.getId());
        return user;
    }

    @Secured({"ADMIN"})
    @PostMapping
    public void addUser(@RequestBody User user) {
        userService.addUser(user);
    }

    @Secured({"ADMIN"})
    @DeleteMapping(path = "{id}")
    public void deleteUser(@PathVariable("id") Long id) {
        userService.deleteUser(id);
    }

    @Secured({"ADMIN"})
    @PutMapping
    public void updateUser(@RequestBody User user) {
        userService.updateUser(
                user.getId(),
                user.getName(),
                user.getEmail(),
                user.getPassword(),
                user.getRoles(),
                user.getBirthDate()
        );
    }
}
