package com.example.perfumestore.user;

import com.example.perfumestore.order.Order;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.Period;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Entity
@Table(name = "user")
@Data
//@EqualsAndHashCode(exclude = "orders")
@NoArgsConstructor
public class User implements UserDetails {
    @Id
    @SequenceGenerator(
            name = "user_seq",
            sequenceName = "user_seq",
            allocationSize = 1
    )
    @GeneratedValue(
            strategy = GenerationType.SEQUENCE,
            generator = "user_seq"
    )
    private Long id;
    private String name;
    private String email;
    private String password;
    @ElementCollection(fetch = FetchType.EAGER)
    @CollectionTable(name = "roles")
    private Set<String> roles;
    private LocalDate birthDate;

    @Transient
    private Integer age;

//    @OneToMany(cascade = CascadeType.REMOVE, fetch = FetchType.EAGER, orphanRemoval = true, mappedBy = "user")
//    private Set<Order> orders;

    public User(String name, String email, String password, LocalDate birthDate, Set<String> roles) {
        this.name = name;
        this.email = email;
        this.password = password;
        this.birthDate = birthDate;
        this.roles = roles;
    }

    public Integer getAge() {
        return Period.between(this.birthDate, LocalDate.now()).getYears();
    }

    @JsonIgnore
    public Boolean isAdmin() {
        return this.roles.contains(adminRoleName());
    }

    @JsonIgnore
    public Boolean isCustomer() {
        return this.roles.contains(customerRoleName());
    }

    @JsonIgnore
    public static String customerRoleName() {
        return "CUSTOMER";
    }

    @JsonIgnore
    public static String adminRoleName() {
        return "ADMIN";
    }

    @JsonIgnore
    public static Set<String> AllRoles() {
        return Set.of(adminRoleName(), customerRoleName());
    }

    @Override
    @JsonIgnore
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return roles.stream().map(SimpleGrantedAuthority::new).collect(Collectors.toList());
    }

    @Override
    @JsonIgnore
    public String getUsername() {
        return this.name;
    }

    @Override
    @JsonIgnore
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    @JsonIgnore
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    @JsonIgnore
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    @JsonIgnore
    public boolean isEnabled() {
        return true;
    }


}
