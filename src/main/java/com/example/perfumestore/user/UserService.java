package com.example.perfumestore.user;

import com.example.perfumestore.order.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.transaction.Transactional;
import java.time.LocalDate;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Service
public class UserService {

    @Autowired
    private PasswordEncoder passwordEncoder;

    private final UserRepository userRepository;
    private final OrderService orderService;
    private final Integer NAME_MINIMUM_LENGTH = 4;
    private final Integer PASSWORD_MINIMUM_LENGTH = 4;
    private final LocalDate MINIMUM_BIRTH_DATE = LocalDate.of(1900, 1, 1);
    private final Pattern email_pattern = Pattern.compile("^(.+)@(.+)$");

    @Autowired
    public UserService(UserRepository userRepository, @Lazy OrderService orderService) {
        this.userRepository = userRepository;
        this.orderService = orderService;
    }

    public List<User> getUsers() {
        return userRepository.findAll();
    }

    public User getUser(Long id) {
        return userRepository.findById(id).orElseThrow(() -> new IllegalStateException("User doesn't exist"));
    }

    public User addUser(User user) {
        Optional<User> exist = userRepository.findByEmail(user.getEmail());
        String email = user.getEmail();
        String password = user.getPassword();
        String name = user.getName();
        LocalDate birthDate = user.getBirthDate();
        Set<String> roles = user.getRoles();

        if (exist.isPresent()) {
            throw new IllegalStateException("Email already used");
        }
        if (name == null || name.isBlank() || name.length() < NAME_MINIMUM_LENGTH) {
            throw new IllegalStateException("user name must have at less " + NAME_MINIMUM_LENGTH + " of length");
        }
        if (password == null || password.isBlank() || password.length() < PASSWORD_MINIMUM_LENGTH) {
            throw new IllegalStateException("user password must have at less " + PASSWORD_MINIMUM_LENGTH + " of length");
        }
        Matcher matcher = email_pattern.matcher(email);
        if (!matcher.matches()) {
            throw new IllegalStateException("user email must have valid email format (e.g. exmaple@dmian.com)");
        }
        if (roles.isEmpty() || !User.AllRoles().containsAll(roles)) {
            throw new IllegalStateException("User must have at less one role (available roles : " + User.AllRoles());
        }
        if (birthDate.isBefore(MINIMUM_BIRTH_DATE)) {
            throw new IllegalStateException("User birth of date cannot be before " + MINIMUM_BIRTH_DATE);
        }
        return userRepository.save(new User(name, email, passwordEncoder.encode(password), birthDate, roles));
    }

    @Transactional
    public void updateUser(Long id, String name, String email, String password, Set<String> roles, LocalDate birthDate) {
        User user = userRepository.findById(id)
                .orElseThrow(() -> new IllegalStateException("User with id " + id + " doesn't exist"));
        if (name != null && !name.isBlank() && name.length() >= NAME_MINIMUM_LENGTH && !Objects.equals(name, user.getName())) {
            user.setName(name);
        }
        if (password != null && !password.isBlank() && password.length() >= PASSWORD_MINIMUM_LENGTH) {
            user.setPassword(passwordEncoder.encode(password));
        }
        if (email != null) {
            Matcher matcher = email_pattern.matcher(email);
            if (matcher.matches() && !Objects.equals(email, user.getEmail())) {
                Optional<User> by_email = userRepository.findByEmail(email);
                if (by_email.isPresent()) {
                    throw new IllegalStateException("Email already used");
                }
                user.setEmail(email);
            }
        }
        if (roles != null && !roles.isEmpty() && User.AllRoles().containsAll(roles)) {
            user.setRoles(roles);
        }
        if (birthDate != null && birthDate.isAfter(MINIMUM_BIRTH_DATE)) {
            user.setBirthDate(birthDate);
        }
    }

    @Transactional
    public void deleteUser(Long id) {
        Optional<User> exist = userRepository.findById(id);
        User user = exist.orElseThrow(() -> new IllegalStateException("User doesn't exist"));
        if (user.isAdmin()) {
            throw new IllegalStateException("Admin user cannot be deleted");
        }
        orderService.deleteUserOrders(user.getId());
        userRepository.deleteById(user.getId());
    }
}
