package com.example.perfumestore.order;

import com.example.perfumestore.user.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "api/order")
public class OrderController {

    private final OrderService orderService;

    @Autowired
    public OrderController(OrderService orderService) {
        this.orderService = orderService;
    }

    @Secured({"ADMIN"})
    @GetMapping
    public List<Order> getOrders() {
        return orderService.getOrders();
    }

    @Secured({"ADMIN"})
    @GetMapping(path = "/user/{id}")
    public List<Order> getOrdersById(@PathVariable("id") Long id) {
        return orderService.getOrdersByUserId(id);
    }

    @Secured({"ADMIN", "CUSTOMER"})
    @GetMapping(path = "my-orders")
    public List<Order> getMyOrders(@AuthenticationPrincipal User user) {
        return orderService.getOrdersByUserId(user.getId());
    }

    @Secured({"ADMIN", "CUSTOMER"})
    @PostMapping
    public void addOrder(@AuthenticationPrincipal User user, @RequestBody Order order) {
        orderService.addOrder(new Order(user, order.getProducts()));
    }

    @Secured({"ADMIN"})
    @DeleteMapping(path = "{id}")
    public void deleteOrder(@PathVariable("id") Long id) {
        orderService.deleteOrder(id);
    }
}
