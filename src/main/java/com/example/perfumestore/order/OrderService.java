package com.example.perfumestore.order;

import com.example.perfumestore.product.Product;
import com.example.perfumestore.product.ProductService;
import com.example.perfumestore.user.User;
import com.example.perfumestore.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class OrderService {

    private final OrderRepository orderRepository;
    private final UserService userService;
    private final ProductService productService;

    @Autowired
    public OrderService(OrderRepository orderRepository, @Lazy UserService userService, @Lazy ProductService productService) {
        this.orderRepository = orderRepository;
        this.userService = userService;
        this.productService = productService;
    }

    public List<Order> getOrders() {
        return orderRepository.findAll();
    }

    public List<Order> getOrdersByUserId(Long id) {
        return orderRepository.findByUserId(id);
    }

    public Order addOrder(Order order) {
        if (order.getId() != null) {
            throw new IllegalStateException("Cannot specify order id when creating a new order");
        }
        User user = userService.getUser(order.getUser().getId());
        Set<Long> products_ids = order.getProducts().stream().map(Product::getId).collect(Collectors.toSet());
        Set<Product> products = productService.getProducts(products_ids).stream().collect(Collectors.toSet());
        if (products.isEmpty()) {
            throw new IllegalStateException("Order must have product(s)");
        }
        return orderRepository.save(new Order(user, products));
    }

    public void deleteOrder(Long id) {
        orderRepository.findById(id).orElseThrow(() -> new IllegalStateException("Order with id " + id + " doesn't exists"));
        orderRepository.deleteById(id);
    }

    public List<Order> deleteUserOrders(Long id) {
        return orderRepository.deleteByUserId(id);
    }
}
