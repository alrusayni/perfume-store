package com.example.perfumestore;

import com.example.perfumestore.order.Order;
import com.example.perfumestore.order.OrderRepository;
import com.example.perfumestore.order.OrderService;
import com.example.perfumestore.product.Product;
import com.example.perfumestore.product.ProductRepository;
import com.example.perfumestore.product.ProductService;
import com.example.perfumestore.user.User;
import com.example.perfumestore.user.UserRepository;
import com.example.perfumestore.user.UserService;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.time.Month;
import java.util.List;
import java.util.Set;

@Component
public class Seed implements InitializingBean {
    @Autowired
    PasswordEncoder passwordEncoder;

    @Autowired
    UserService userService;
    @Autowired
    OrderService orderService;
    @Autowired
    ProductService productService;


    @Override
    public void afterPropertiesSet() {
        // Users
        User admin = userService.addUser(new User(
                "admin",
                "admin@gmail.com",
                "admin",
                LocalDate.of(1995, Month.DECEMBER, 18),
                Set.of(User.adminRoleName())
        ));
        User tester = userService.addUser(new User(
                "test",
                "test@gmail.com",
                "test",
                LocalDate.of(1995, Month.DECEMBER, 18),
                Set.of(User.customerRoleName())
        ));

        // Product
        Product bag = productService.addProduct(new Product(
                "Bag",
                "High quilty bag",
                Set.of("Black", "22 Liter"),
                99
        ));

        Product laptop = productService.addProduct(new Product(
                "Laptop",
                "Best laptop on earth you can ever find, try it out!",
                Set.of("Black", "14 Inch", "22 hours working time"),
                1999
        ));

        // Orders
        orderService.addOrder(new Order(tester, Set.of(bag)));
    }
}
