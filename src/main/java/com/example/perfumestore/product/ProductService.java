package com.example.perfumestore.product;

import org.hibernate.query.criteria.internal.expression.function.AggregationFunction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;

@Service
public class ProductService {

    private final ProductRepository productRepository;
    private final Integer MINIMUM_LENGTH = 2;
    private final Integer MINIMUM_PRICE = 0;

    @Autowired
    public ProductService(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    public List<Product> getProducts() {
        return productRepository.findAll();
    }

    public List<Product> getProducts(Set<Long> products) {
        return productRepository.findAllById(products);
    }

    public Product addProduct(Product product) {
        Optional<Product> exists = productRepository.findByName(product.getName());
        if (exists.isPresent()) {
            throw new IllegalStateException("Product name already used");
        }
        if (product.getName() == null || product.getName().isBlank() || product.getName().length() < MINIMUM_LENGTH) {
            throw new IllegalStateException("Product name must have at less " + MINIMUM_LENGTH + " characters");
        }
        if (product.getDescription() == null || product.getDescription().isBlank() || product.getDescription().length() < MINIMUM_LENGTH) {
            throw new IllegalStateException("Product description must have at less " + MINIMUM_LENGTH + " characters");
        }
        if (product.getPrice() == null || product.getPrice() <= MINIMUM_PRICE) {
            throw new IllegalStateException("Product must have valid price between " + MINIMUM_PRICE + ".." + Integer.MAX_VALUE);
        }
        return productRepository.save(new Product(product.getName(), product.getDescription(), product.getSpecs(), product.getPrice()));
    }

    public void deleteProduct(Long id) {
        productRepository.findById(id).orElseThrow(() -> new IllegalStateException("Product with id " + id + " doesn't exists"));
        productRepository.deleteById(id);
    }

    @Transactional
    public void updateProduct(Long id, String name, String description, Integer price, Set<String> specs) {
        Product product = productRepository.findById(id).orElseThrow(() -> new IllegalStateException("Product with id " + id + " doesn't exists"));
        if (name != null && name.length() >= MINIMUM_LENGTH && !Objects.equals(name, product.getName())) {
            product.setName(name);
        }

        if (description != null && description.length() >= MINIMUM_LENGTH && !description.isBlank() && !Objects.equals(description, product.getDescription())) {
            product.setDescription(description);
        }

        if (price != null && !Objects.equals(price, product.getPrice()) && price >= MINIMUM_PRICE) {
            product.setPrice(price);
        }

        if (specs != null && !Objects.equals(specs, product.getSpecs())) {
            product.setSpecs(specs);
        }
    }
}
