package com.example.perfumestore.product;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.function.Function;
import java.util.function.Predicate;

@RestController
@RequestMapping(path = "api/product")
public class ProductController {
    private final ProductService productService;

    @Autowired
    public ProductController(ProductService productService) {
        this.productService = productService;
    }

    @Secured({"ADMIN", "CUSTOMER"})
    @GetMapping
    public List<Product> getProducts() {
        return productService.getProducts();
    }

    @Secured({"ADMIN"})
    @PostMapping
    public void addProduct(@RequestBody Product product) {
        productService.addProduct(product);
    }

    @Secured({"ADMIN"})
    @DeleteMapping(path = "{id}")
    public void deleteProduct(@PathVariable("id") Long id) {
        productService.deleteProduct(id);
    }

    @Secured({"ADMIN"})
    @PutMapping
    public void updateProduct(@RequestBody Product product) {
        productService.updateProduct(
                product.getId(),
                product.getName(),
                product.getDescription(),
                product.getPrice(),
                product.getSpecs()
        );
    }
}
